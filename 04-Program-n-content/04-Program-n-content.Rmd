# (PART) Conference areas {-} 


# Program and Content {#program-content}

Program and Content is the largest area of the conference. This team invites the keynote speakers, invites and coordinates the [program committee] that evaluates submitted work, prepares all the requirements for each format of the conference, and defines the final schedule. 

This section refers to the technical content and the preparation of all formats of the conference. The components of the conference agenda that correspond to non-technical/non-academic content but to social and networking events are in the [Social events](#social-events) section of this knowledgebase:
the [recreational activities](#recreational-activities),
the [networking sessions](#networking-sessions),
the [satellite](#satellite-events), and [community events](#community-events). 

<!-- ## Work dynamic -->

<!-- * It is better to have one person leading each of these components. In our case, it was not always possible.  -->
<!-- * Because there was so much to do and to follow, the team had biweekly meetings, either written sprints on Slack or zoom calls. Always with an agenda and a fixed amount of time to discuss each point. Whenever something would require more time to think and solve, we would create an issue and set a deadline for it.  -->
<!-- The team was lead by two global coordinators in 2021 (RJ, DHP), and the tasks had one or two responsible people for them. The specific tasks are listed as follows:  -->

## Formats of the conference {#formats-conference}

### Keynotes 

Keynote talks are one-hour talks by prominent members of the R community. They can be individual or in group (in which case they last more than an hour). Two slots are usually reserved to R Core Team members.  

The Program and Content committee has to make a list of possible subjects and speakers. Each proposal has to be accepted by the R Foundation. Take special attention to diversity and representation in the keynote speakers and subjects. 

The committee should designate one or several "points of contact" with the Keynote speakers, people who will be responsible for the communication, registration, and preparation of the speaker. Since Keynote speakers are usually invited to the conference they do not go through the usual registration process. They should nonetheless be registered to receive all the access credentials and communications.

The points of contact are also responsible for remembering the Keynote speakers about the [Code of conduct](#coc) of the conference and retrieving the links or files for their [presentation materials](#materials-availability). 


### Regular talks

Regular talks are the backbone of every useR!, with high-quality contributions from different fields. The talks are limited to 15-20 minutes<!--ö check other years time and structure--> followed by a short Q&A time-slot. Regular talks are organized in parallel sessions with 4-5 talks.

In online and hybrid settings, talks can be either live or pre-recorded, with a live Q&A part to encourage engagement of the conference participants. 
The online format allowed for talks being presented in languages other than English, as the talks can be accompanied by subtitles. The speakers only had to provide an English transcript.

In-person conferences recorded the videos and published them after the conference. In online and hybrid settings, a live streaming of either the pre-recorded or the live talks should be provided, and an edited and captioned recording should be published after the conference. 

The __Call for Abstracts__ should give details about language, video and slide formats, and accesibility guidelines for preparing the materials.

Collaborate with the [communication](#communication-and-promotion) team to promote the Call for Abstracts in all social media accounts of the conference.

After the reviews by the [Program Committee](#program-committee), the Program and Content team members should designate some members to shortlist the abstracts and decide and communicate the decisions about the final acceptance. 

### Poster sessions/lightning talks/elevator pitches

Any work related to R that does not fit the regular talk category has a place in the lightning talks and poster sessions.

In person editions of useR! had lightning talks, shorter than the regular, and poster sessions. With the advent of online formats, these sessions were reformatted. At useR! 2020, lightning talks were ~5 minute sessions and poster sessions were ~3 minute talks, with the option to submit the poster as supplementary material. useR! 2021 merged these two formats into *Elevator pitches*, that could be presented in two different forms: a written **technical note** or a **lightning talk**. Lightning talks were 5 minutes long. Technical notes contained some text and graphs--like posters--and were expected to be short and readable in 5 minutes.

The poster sessions/lightning talks sessions are scheduled throughout the conference and designated Q&A sessions can be hosted to allow for direct interactions with the presenters.

In useR! 2021, these contributions could be in a language different from English as long as the speaker provided an English transcript or translation.

### Tutorials

Every year, useR! features a tutorial program addressing the diverse interests of its audience. The tutorials are aimed at R users/developers worldwide who wish to learn about new technologies or enhance their knowledge about the existing technologies and novices to the `#Rstats` world interested in introductory lessons.  Tutorials have helped the R user community to be updated about the latest packages, concepts, and best practices in R.

Tutorial sessions occupy one day of the conference, and in online settings, are hosted in different days, before, during, or after the conference. For useR! 2021, a whole day, spanning three timezones, was dedicated to all tutorials, giving the attendees the opportunity to take a break during the week and assist to their tutorials. 

The selection of tutorials follows a similar timeline than abstract submission, with a call for proposals, a period for submissions, and a review by the Program Committee. Due to the infrastructure needed to host a tutorial--both in in-person and in online settings--participants must register to attend so there is a separate call for registration.

Check the detailed guide for [tutorial organization](#tutorial-organization)

### Panels and Incubators

Panels are suitable for topics that are of interest to a large audience. The idea behind the panels is to engage in a 60-minute topic discussion with experts/stakeholders. Interaction with the audience (e.g., a moderated Q&A) is possible. Incubators, on the other hand, address specific topics that are of interest to a smaller group of people working in the same field. They have a specific aim (i.e., formulate ten principles for good XX) and are not mere networking sessions.

Proposing a panel or incubator at useR! 2021 followed the same guidelines as abstracts and tutorials, and proposals were reviewed but members of the Program Commitee. 


## Program Committee

The program committee (PC) is the group of specialists that review the submissions for the conference and, given the criteria for evaluation, give a recommendation for acceptance or rejection of the proposal. 

The Program Committee should have a head, dedicated to the organization of this crucial phase of the conference. The head of the Program Committee coordinates the preliminary list of reviewers, invites them, and assigns them the submitted proposals. They have to make sure the PC composition is balanced accross topics, areas, and representation accross regions, gender, and race. Avoid creating an all-men or all-white Program Committee!

The nomination of members of the PC has to be approved by the R Foundation. A list of potential members of the PC will include pre-approved members and some replacement members in case the first options decline the invitation. 

The PC lead works in coordination of the technology team to give access to PC members to the submission platform and assign them submitted proposals. Following up with the PC members when the deadline is close is necessary, as well as checking for any missing review. Each proposal should be reviewed by at least three members of the Program Committee, but sometimes there are exceptions to this rule. 

Final decisions of the accepted abstracts are based on the reviews of the program committee and are done in coordination with the Global Organizers. A check for balance accross topics, knowledge areas, geographic regions, gender, and race of the presenters is necessary. 


## Final schedule

After all the keynotes, regular talks, and lightning talks/posters have been accepted, they should be organised by topics in parallel sessions per day, with one or two Keynotes per day. 

The final schedule should be posted when the conference registration starts, or shortly thereafter. The [communication](#communication-and-promotion) team should prepare content to promote all the components of the schedule.


## Awards and mentions

An award session for Early Career Researchers (students, postdocs, and other early career trainees in industry) is usually held during the [closing ceremony](#opening-and-closing-ceremonies). Other key aspects of the conference can be awarded as well. For example, useR! 2021 gave Accessibility awards and recognized some key people that are behind the wheels of R. 

To evaluate the presentations, a definition of the _scope_ of the award (what presentations and which presenters will be considered) is necessary. Then, a team with reviewers--ideally but not necessarily from the Program Committee-- should be created to attend in-person or watch the presentations. Some criteria of what will be evaluated should be clearly defined.


