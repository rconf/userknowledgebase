# UseR! Social media policy {#social-media-policy}
<!-- how useR and the organizing team should conduct themselves online --> 
<!-- maintain your voice while mitigating social media risks. --> 

A social media policy is intended to help use all the social media accounts of useR! in a safe and meaningful way and to mitigate social media risks. 

This social media policy presents
[general guidelines](#general-principles) about social media usage, safety, and etiquette,
[account management basics] (scope and use of the social media accounts),
[content and schedule guidelines](#content-and-schedule),
and [content promotion guidelines](#content-promotion). 

 <!-- 
+ [Account management basics]
+ How to post?
  - Meaningful, supportive and constructive
  - Accessible
  - Lawfully. 
  - Try to instruct the organizers to taks care of their digital personas, remember everyone if bound to the Code of conduct and that behaviors outside the spaces of the conference are valid elements of analysis 
+ Have a plan in case the Code of conduct is breached or there is backlash for some postings
  + A plan of action for a security or PR crisis
+ What to post?(content)
+ When to post?
+ Promoting content
        - As a useR! organizer, interact with the account, repost their content, repost translations if they are not posted originally
+ After the conference: review and update this social media policy.
    - What did not work, what did work, what is missing? what are things we learned that belong here? --> 

## General principles
<!--purpose of this section: General management of the Twitter/other social media account: good etiquette and  practices-->


### Responsibles

First of all, assign responsible people for the social media aspects of the conference. They will be most likely members of the team who is in charge of other aspects of the conference [communication](#communication-and-promotion). Share the accounts passwords and make sure that these are kept safe, change them when necessary.

### Etiquette and Code of conduct

Social media are spaces of the useR! conference and as such, the [Code of Conduct](#coc) applies to them. 
For this reason, it is extremely important to understand the Code of Conduct documentation and procedures and be prepared to enforce it from the start of the interaction with the community.

#### Precautions 

Social media should be used in order to minimize the risk of unwanted behavior and Code of Conduct violations. Some actions in this regard include: 

- Obtain approval from other organizations or members of the taskforce if you are tweeting news about them/on their behalf (not simply re-tweeting)
- Have precaution and do not follow or retweet accounts that present disturbing behavior, look like bots or incitate hate speech.
- Avoid the promotion of unhealthy/exclusionary culture, insider jokes and heavy technical jargon
-Make a timely response to community concerns when an error is made (for example, if a community group has concerns about an account that was retweeted)

#### In case of a CoC violation 

Follow the Code of Conduct report guidelines

- Take prints for documentation purposes, and undo retweets or delete interactions that are harmful to the community
- Make a Code of conduct report to the Code of Conduct team in case the Code of conduct is breached during interactions in the useR! accounts


## Account management basics

### Scope of the useR! accounts

userR! has Twitter and LinkedIn accounts, to include regions where usage of one of those social media platforms is low. 

### Account management

__Who to follow__

- R Foundation
- Community partners (including R Consortium, Forwards, RLadies, miR, latinR, AfricaR)
- Institutional partners (university hosts, supporting academic societies)
- Any individual that has contributed to the organization of the conference (organization team, program committee, potentially other helpers)
- Individuals that are active in the international R community and have given visibility to useR! and other R-related content in the past
- Do not follow commercial sponsors or vendors as these change from year to year and we may not want to give them an infinite endorsement

__Who/when to unfollow__

- Unfollow in cases that an account shows evidence of behavior counter to our mission of creating a safe and inclusive conference
- Revert retweets of tweets from that account and hide replies to those retweets
- Criticism of useR! is not necessarily a reason to unfollow

*At least two Twitter moderators should agree that unfollowing is necessary*

__Who/when to block__

Blocking can be more helpful than unfollowing as our account will show any blocked accounts and therefore we keep the fact that action was necessary in our institutional memory.

- Block in cases that an account shows particularly unpleasant behavior, e.g. offensive tweets.
- Revert retweets of tweets from that account 
- Report the account if the behavior is abusive

*At least two Twitter moderators should agree that blocking is necessary*

__Who/when to mute__

Muting can be useful to down-weight irrelevant content or accounts that we respect but that already have a lot of exposure on `#rstats` Twitter. This will help to surface relevant content from under-represented groups.

- Mute accounts that use clashing hashtags (this can sometimes happen with events)
- Mute #rstats accounts with especially high follower numbers
- Mute accounts from respected community members that tweet about non-data science content with high frequency

*At least two Twitter moderators should agree that muting is necessary*. Note that this will affect what other Twitter moderators can see when they are logged in!


__What to like__

- Like rather than retweet to avoid over-sharing on same topic/from same account
- Like responses to our tweets/re-tweets
- Remember _likes in Twitter are public and can be seen by anyone_ so they work as soft retweets

__Who/what to reply__

- Thank people as appropriate
- Try to avoid getting into Twitter wars
- Check whether an account is a member of the data science/tech community acting in good faith before responding
- Check with other Organizing Team members how to respond if unsure
- *Be careful who is cc'd on replies* - do not add to harm by replying to trolls who have cc'd other people
- Answer all the questions that arise in social media

## Content and schedule

 <!-- (before, during, after the conference) --> 

### Social Media Content format

Have a list of suggested post following the main milestones. All suggestions should contain the following information:

* __Content__ (respecting character length for each platform)
* Appropriate __hashtags__:
    + include **`#rstats`** to improve the reach
    + include the conference hashtag **`#useR2021`** to help tracking our posts
    + do not use too many hashtags
    + capitalize using `#CamelCase` for screen reader accessibility
    + include language specific hashtags when needed `#RStatsES`, `#RStatsFR`
* __Emojis__: Do not use more than 1-2 emoji (if at all) because the emoji’s alternative text is read out by screen readers. Check out the alternative text for the emoji to see if it fits context and intention
* __Fonts__: Do not use fancy modified fonts as screen readers read out each character
* Add __visuals__ (images or GIF) to most tweets. Add useR! branding in images posted, for example in images portraying keynote speakers.
* Include __alt-text in every image or GIF posted__. If posting a video it shoud be captioned.
* Choose some people to be mentioned or tagged, if any (but avoid being spammy)
* Times with timezones, always including UTC.
* Official posts should be written in English with occasional, planned exceptions to involve other communities. When doing so, indicate the language of the tweet by adding an acronym ("ES:", "FR:", "PT:")

Example:

|  |  |
|--|--|
|text| Got fear of missing out? This list is for you. Find a list of key deadlines on the roadmap to #useR2021 ! #RstatsEN #FOMO _LINK_TO_KEYDATES_SITE |
| languages | EN |
| image | marmot on the look out |
| alt-text | useR mascot Margot is on the look out to avoid missing important deadlines |
| schedule | Jan 12, 2021 09:05 a.m. UTC |


### Posting schedule 

Content creation takes time and needs revision, so it's better to create a posting schedule. The key dates are a good guide on what to post about. 

Schedule posts to be published at prime time to maximize their reach

As a guide, the posting schedule from useR! 2020 included the following content:

__Before the conference__

- When and where the conference is held
- The website and how to communicate with the team
- About the organizing team
- Confirmed keynotes, their titles
- Call for presentation of tutorials
- Reminder about the call for tutorials when the closing date is near
- About the possibility of being a sponsor
- About confirmed sponsors, thanking, and inviting to be sponsors
- The closure of the call for tutorials
- Results of the review of the tutorials
- Thank the reviewers of the tutorials
- About the call for abstracts
- Reminder of the call for abtracts deadline
- Explaining each type of submission that can be done
- On the closing of the call for works
- About the institutions that support the conference
- About the Scientific Committee
- Results of the work reviews
- Thank the reviewers of the papers
- About accessibility policies, diversity, etc.
- When partners make an event to promote useR!
- The opening of registration
- FAQs about registration
- The opening of scholarships
- FAQs about scholarships
- News of the agenda and events
- Greetings to the community on special dates (eg: pride day, trans visibility day, etc.)
- The complete agenda prior to the conference

__During the conference__

- Communicate 15-30 minutes before each event occurs that it is about to occur. This can be pre-programmed when the final agenda is ready
- Communicate any change on the schedule
- [Live tweet](#live-tweeting) the keynotes in threads
- Make the post agreed with the sponsors
- Answer all the questions that appear
- Retweet messages from the people who are participating

__After the conference__

- Share FAQs on next steps (certificate, videos, slides, etc)
- Announce award winners
- Share links to surveys
- Continue RT or liking the mentions of the people who participated

### Live tweeting

There are conference events, such as keynotes, that deserve to have an impact on social media as they happen. Here are some considerations for organizing and conducting this live tweeting:

1. Agree if the event will be covered in more than one language. If so, it is recommended to have one person per language
1. Find the people for the task: contact people who feel comfortable live tweeting events. They can be people you have seen live tweeting, people who present the information in a way you seem fit, or people who know about the topic of the event and are used to each social network.  The [\@WeAreRLadies](https://twitter.com/WeAreRLadies) account is a good source of potential people to do this job. Ask them in advance if they would like to live tweet the event.
1. Agree on whether to give them account credentials or [TweetDeck](https://tweetdeck.twitter.com/) access to perform the task. Arrange whatever is necessary to get everything working before the event accordingly.
1. Share the [guidelines](#social-media-policy) for social media posts with those doing the work. Remember important points such as the use of __alternative text in any image or GIF__, not using images or GIFs of boys or girls, use of [inclusive language], use of the official hashtags of the conference, etc.

If you are going to live-tweet an event: 

1. Introduce the keynote: title, speaker, links to follow the stream, and link to slides/other material if they were shared. Tag the speaker in the post.
1. Posts highlighting the important points, best if accompanied by __screenshots of the slides__ or the conference video as it happens. If you have the original material you can copy the text of the corresponding slide and add it as __alt-text__, otherwise, you should describe what the screenshot contains. In the text of the tweet, the most important thing is highlighted, it can be a phrase or title.  Use some emojis.
1. Share the different resources mentioned: websites, packages, materials, videos, etc.  Again, they can be copied from the materials if they are available, if not they can be searched on the web or copied from the talk to be shared.
1. If there are __calls to action__ by the speaker, highlight them.
1. If there are __tips__ or __rules__, put together one or two posts summarizing them.
1. __Questions/answers__ that you find interesting.  Add links or materials mentioned in the answer.
1. Thank the keynote speaker at the end, share the materials again
1. For each post, in addition to the official hashtags, think of specific ones for the topic.
1. Someone else from the communication team can engage/retweet from other attendees during the keynote. This has to be agreed in advance.
1. Publicly thank those who did this work. Incorporate them as part of the communication team.  Remove permission of the account if they will not do this work again.


## Promoting content {#content-promotion}

Share the content with the organizing team members, partner communities, and sponsors to ask for sharing the posts (RT).

* The language of our posts is friendly and inclusive and limits insiderism where possible.   
* Do not start a tweet with an @ mention, otherwise Twitter treats the tweet as a reply - you can get round this by starting with a period, e.g. “.@mention”.
* tweets will be posted on linkedIn as well
* If we don't have 5 post at day, twitter does not include us in their algorithms


## Revision and updating of this policy

After the conference, update and review this policy for future teams, based on what you learned, what worked and what did not, and on the changing context of social media usage.