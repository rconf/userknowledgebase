## CoC Team guidelines {#coc-guidelines}

::: warning
__Main links - quick access__

+ Our e-mail is: useR2021-coc@r-project.org
+ CoC Drive folder
+ Schedules: detailed groups and times in our Sheets (and team contact information); useR! Team schedule (marmots, hosts, chairs, CoC team); conference schedule
+ __Incident report template form__
+ Lounge credentials (Lounge link during the conference: https://join.user2021.r-project.org/#/)
+ Guide to the Lounge and some useful commands we have discussed for our team.
+ Some pre-formatted texts
+ Jitsi room for us to meet if necessary https://meet.jit.si/CoC_team_useR_2021
Telegram group for emergency
:::

__Before the conference__

+ Know and fully understand our CoC. Be able to explain it to other people, if asked. https://user2021.r-project.org/participation/coc/
+ Read the Frame Shift Consulting book and be aware of these guidelines and of the incident report template form: propose changes, discuss any doubts on how to enforce the CoC, how to take a report, how to make a decision, etc.
+  Fill your schedule and know who you share a group with. Each group has 3-4 people available during a similar 12 hour period. You are not expected to be available during these 12 hours (and there is overlap between groups), but you'll probably be in more contact with people in the same group. Remember who are your group colleagues, you will gather with them in case of taking a report! (but also: may need to call a person from other groups).
+ Schedule. Fill your schedule in your sheet and check it in TeamUp 
+ Train on the Lounge usage. Have your CoC team credentials on hand. 
+ Know who has access to the email in your group. (Natalia, Adithi or Sara)

__During the conference__

+ Be connected with your team in our private Slack channel. Slack is the main channel and telegram should be used in case of emergencies. 
In particular, be connected with the 2-3 persons in your group sharing timezone and availability.
+ In your shift. Be alert to:
    + Lounge notifications (log-in with CoC credentials in a private incognito session),
    + Lounge DMs sent to you (as a member of a CoC team)
    + E-mail notifications if you have access  useR2021-coc [at] r-project.org.
+ Know your useR! Team schedule (to be able to know who to ask for help, e.g who are the MaRmots in your shit) and the conference schedule. In Slack, you may join the channels #marmots and #zoom-hosts to ask help from Marmots and hosts or be available for them.
+ CoC should be silent in the Lounge, but available to respond to messages and to receive incident reports (CoC_team should not appear in the midst of regular conversations. no talking from CoC_team). Be able to quickly share our CoC if requested (web or /coc command in Lounge), and to clarify any doubts on it. 
+ You should be able to attend the conference normally and interact with others using your own personal profile, since CoC_team is a silent user. If you feel the need to intervene, deflect, interrupt any conversation, you can do this as a member of the organizing team and as an active bystander. This is important because we want to avoid a “policive” atmosphere inside the Lounge, where CoC_team appears, chats, gives their opinion and interrupts. The maRmots received the instructions to intervene - let them handle the situations, support them, and talk with them if needed (via CoC_team if necessary).

__In case a report is received__

+ If an incident report is received, follow our template form step-by-step: gather with your   CoC small team (the group defined by the schedule), collect the required information, and work on a decision. 
+ Remember the 2hr deadline for a first response and the 24hr deadline to analyze the case you are on. 
+ If an incident report is received by another person, you may be requested (if you are able) to be part of a small CoC team and work on a report, even if you are not on that shift. Shifts and groups are flexible! 
+ Don’t hesitate to contact Natalia, Sara and Andrea in case you need anything or have doubts
+ Well-being: take care of yourself and your well-being! Rest, ask a CoC member to replace you if you feel unsafe, if you feel uncomfortable with handling a report, or if you are too tired to deal with a task. Be alert if a peer is about to burn out. Be kind to anyone on the team. Thank the reporters, thank your peers in the CoC team, and thank any other person in the useR! organizing committees that are helping your team.

__After the conference__

+ If a report was received, work with your small team on an anonymized version to share with the full CoC team and with the useR! Organizers (transparency report). 
+ Save any important message history from the Lounge (it will be deleted ~2 weeks after the conference). Where? In our Google Drive folder. How? Zip all the important information and save the zipped folder named with the report incident number.
+ Participate in a CoC meeting for a final balance of our work as a team.


