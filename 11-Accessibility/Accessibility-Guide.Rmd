
<!-- https://raw.githubusercontent.com/forwards/event_best_practices/master/accessability_guidelines_user20202lh.MD -->
<!-- https://gitlab.com/user-2021-team/user-2021-global/-/edit/master/diversity/accessibility/accessibility_standards.md -->



Avoid dividing people into groups based on color-coded items.
Use additional sensory hints like "Group One is Blue."

Avoid bright and/or flashing lights or images that could trigger seizures.

Provide means for people to position themselves more flexibly than in standard chairs. Pillows and footrests are helpful for chairs. Yoga mats are helpful if sitting in chairs for long periods is difficult.





####  References

Lots of people talked to me about what inclusion would mean for them.
Some of these ideas, plus others, are discussed in the references below.

Divya Persaud
Resources for Accessibility @ Conferences
https://docs.google.com/document/d/1WSs99ys12OXkkU0WNTNR02rIA-ndRNZVgDeJMnd3j1E/mobilebasic

Gabi Serrato Marks.
How to Make Professional Conferences More Accessible for Disabled People
https://blog.ucsusa.org/science-blogger/how-to-make-professional-conferences-more-accessible-for-disabled-people-guidance-from-actual-disabled-scientists?utm_source=facebook&utm_medium=social&utm_campaign=fb&fbclid=IwAR0V4Yq5QMzhUjvcXeKbCG5SxgAA7QAS7HvHv0Uo9xHMYjDtEfq8R3KI1TM


SigAccess.org
Accessible Conference Guide
http://www.sigaccess.org/welcome-to-sigaccess/resources/accessible-conference-guide/

