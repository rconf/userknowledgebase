## Code of conduct {#coc}

### Introduction

The Code of Conduct is a document that aims to keep the useR! community safe. The CoC states explicitly what is considered unacceptable behaviour, in which spaces of the conference it applies, the consequences for incurring in unacceptable behavior, and the procedure to report a violation. 

To serve its purpose, the Organizing Committee has to gather a __Code of Conduct Response Team__, that will get prepared to enforce the Code of Conduct in all spaces of the conference. The team is responsible for taking reports, investigating, deciding on responses, implementing those responses, and informing the community about their decisions.

We strongly recommend that the members of the whole Organizing Team read at least the first chapter of the book [How to respond to Code of Conduct reports](https://frameshiftconsulting.com/code-of-conduct-book/) by Valérie Aurora and that the Code of Conduct Response Team read the whole book. 
The book is available under the [CC BY-SA 4.0 license](https://creativecommons.org/licenses/by-sa/4.0/), allowing free reuse and modification of the materials as long as you credit the authors.
Part of this knowledgebase and of the work of previous useR! Code of Conduct Response Teams are based on advice from that book. 
For useR! 2021, reading the book was a requisite for being part of the CoC team. 

### useR! Conferences Code of conduct

The R Foundation has a [Code of Conduct Policy](https://www.r-project.org/coc-policy.html) that states that conferences organized by the R Foundation must have a code of conduct and provides [a standard Code of Conduct](https://www.r-project.org/coc.html). The current standard Code of Conduct is based on the version used at useR! 2018.

You can adopt an alternative Code of Conduct or modify the R Foundation standard Code of Conduct, but any changes should be approved by the R Foundation Conferences Committee (RFCC).

For useR! 2021, the CoC Response Team revised and proposed suggestions to the standard text, in order to adjust it for the online format of the conference. Other rewordings were also proposed, and the [final version](#coc-en) was approved by the R Foundation. 

In the most recent version of the Code of Conduct, the Code of Conduct Team added that any Code of Conduct Response Team member _will recuse themselves in case they are part of a report, or have a conflict of interest_. The team also added the possibility of making the report _directly to the RFCCC_ in case the victim does not feel safe even after such a recusation (See [Code of conduct team](#coc-team)), but the team should be large and independent enough to avoid this last resource.


The team also translated the Code of Conduct to [Spanish](#coc-es), [Portuguese](#coc-pt), and [French](#coc-fr), to account for the global nature of the conference.



### Gather a Code of Conduct Response Team {#coc-team}

The code of conduct response team must have the full support of the conference organisation committee and the R Foundation leadership. They must be able to work independently and without interference. Even removals in case one of the members commits a significant mistake should be discussed and decided within the Code of Conduct Response Team, without outer interference, and only go to upper instances in cases of non-resolution.

Enforcing a code of conduct is intense emotional labor, and potential members of the Response Team should be aware of it _before deciding to accept the role_. This also means that looking for the right people is important rather than expecting people to volunteer themselves. The Organizing Team may consider paying the members of the Code of Conduct Response Team due to the emotional charge that enforcing the CoC may represent. 

The CoC team members should understand the importance of the Code of Conduct and its effectiveness, and not have reservations about the need to enforce it. They also must be aware of the several types of oppression that exist, and understand that different types of oppression intersect in complex ways that make it impossible to analyze them independently from one another (i.e., they should be aware of the concept of [intersectionality](https://en.wikipedia.org/wiki/Intersectionality)). They also should not believe that one form of oppression (e.g. classism, sexism) is more important than others. Looking for candidates with diverse backgrounds may be useful to have a team that understands the complexities of insersecting forms of oppression and the power dynamics that underlie most CoC violations. In particular, potential members should be prepared to act when powerful people in the community are reported, and not show more empathy towards the perpetrators than the victims. You might consider preferring team members with previous response support, university Harassment Advisors, social work, or similar industry HR or NGO role. 

The team should be large enough so that it can gather subteams and operate effectively even if a third of its members are not available. It also needs to be large in order to allow members to recuse themselves in case they are a party of a report, or have any relationship with one of the parties. Previously the response team had 3-6 members. During useR! 2021 the team had 11 members, with subgroups assigned to each one of the three timezones.


Having people dedicated exclusively to CoC tasks, and not involved in other areas of the conference–even people from outside the organization team–may reduce the risk of incurring in conflict of interest. Look, for example, among the members of the [Partner Communities](#partner-communities), whose interest in keeping the community safe is already present.

On the other hand, having a person from the Global Coordinators in the group may create a channel of communication between the Code of Conduct Global Team and the Global Coordinators, which can be beneficial, but only if this connection is not used to undermine the work of the Code of Conduct Response team. The Code of Conduct team must be able to choose freely if the presence of a Global Coordinator is desirable or not in their own context. 

Example: The selection for people in the Code of Conduct team at useR! 2021 was first done by asking for volunteers, and then inviting some members of the organising team and the partner communities that represent different groups and regions of the world. The candidates were asked to read the first chapter of the Code of Conduct Response Book, and decide if they were interested in being part of the team. Some one-on-one calls to solve any doubts were made. 

### Preparing the team {#coc-preparation}

The team needs to be trained to enforce the Code of Conduct in the weeks leading up to the event. Some members may have not been a part of response teams, or may have not had to deal with the wide variety of possible incidents. We recommend sharing the [resources](#coc-resources) with them well in advance so they can read sample cases, and get used to your event's Code of Conduct.

<!--We strongly suggest that the first step of the preparation be reading the book [How to respond to Code of Conduct reports](https://frameshiftconsulting.com/code-of-conduct-book/) by Valérie Aurora. At useR! 2021, reading the first chapter of this manual was a condition to consider being part of the CoC response team. In ideal settings, every person in the Organizing Team should read the first chapter if this book. -->

We recommend to do a follow-up discussion, written or verbal as preferred, to address any specific questions or concerns they might have about scenarios, whether it be aggressive language, possible support from the university's legal team, or otherwise. These discussions will help you prepare and adjust based on the specific organisational capabilities and the team's needs.

### Code of Conduct Response team tasks

- Read and discuss the Code of Conduct
- Write [internal guidelines](#coc-guidelines), set response deadlines and [write templates for taking reports](#coc-report-template) and responding to them
- Set up [the e-mail for Code of conduct response](#coc-email) and define who has access, including backups in case the main responsible is not available
- Make sure that the Code of Conduct is [exhibited proeminently](#coc-communication) in several spaces of the conference, the website, the forms, and mentionned at the beginning of sessions 
- Define the internal roles and schedules of the team, who are going to be reporters, contact person, their schedules, subteams if any
- In in-person settings, know which room can be made available to the response team on short notice in case they need to meet or interview, in private
- In online settings, prepare conference helpers to give instructions to any potential reporter on how to contact the Code of Conduct team
- Check if you can get support from the hosts's legal team if needed, and what would that look like during the event of afterwards
- Decide and prepare on a simple way to identify the response team (lanyard, badge, icon on chat platforms, etc.) and communicate this to the conference attendees
- Agree on an internal communication channel between the members (email, phone, messaging app, etc.). In useR! 2021 the same channel that was used for team discussions was set to be the official communication channel, but a backup Telegram group was also created, for emergencies


### Code of Conduct communication {#coc-communication}

The team needs to agree on efficient and appropriate communication channels among the team, and with the organising committee during the event. Consider exchanging phone numbers, setting up a messaging group, using an ever growing email thread, etc.

Lastly, it is good practice to have a face to face meeting of the team on site, preferably a day before the event or the morning of the first day. Hopefully previous discussions have already started to build trust and cooporation among the team. If needed take the time to do some quick team building, and resolve any last minute operational issues.

#### Communication to the attendees {-}

We recommend to encourage, or require, all the conference participants to read and understand the CoC well in advance. Some conference hang signs with the CoC, and the contact details (email, phone number, names) of the response team, but it is not required. We recommend to the Coc Response Team contact details and identifiers in the event's website, and information booklet, as well as mention it in the opening comments to the event. 


#### CoC email for the event {- #coc-email}

The organising committee is recommended to set up a secure stand alone email account for the response team, which can be deleted at a later date. This address should have limited access to respect the privacy of everyone involved. The response team can decide if everyone on the team has access, if for example, they wish to rotate the responsibility of checking it during the event, or if it is the responsibility of 1-2 members only. For useR! 2021, one member per timezone had access to the email account.

### Preparing report-taking guidelines

The Code of Conduct response team should prepare and be familiarized with their documents for report-taking and for [solving each violation](#coc-report-template). This preparation should be done as soon as possible after the team is completed, and not wait for the event. Bear in mind that violations of the Code of Conduct may happen before the event since the spaces of the conference include Social media interactions, email communications, and the organizing team work itself. 

You can check an example for Code of conduct guidelines from useR! 2021 [in the Appendix](#coc-guidelines)

### Resources to prepare team members {#coc-resources}

- [R Foundation CoC Policy](https://www.r-project.org/coc-policy.html)
- [Forwards Guidelines](https://github.com/forwards/foundation/blob/7fe098d0fd82902c91449160487c90f768e4f39c/coc_policy/guidelines.md) for Code of Conduct Policy. This is a draft that has not yet been accepted by the R Foundation.
- This [CoC Response ebook](https://frameshiftconsulting.com/code-of-conduct-book/) contains many useful case studies. Previous response team members found it useful to read it before the event.
- Forwards can share anonimised previous reports with the team, but they are not for public circulation.

#### During the event {#during-coc}

- Team members should check the response team email regularly
- You might want to take turns being responsible for it so you don't feel like you're spending the entire conference worried about checking for new emails

If an incident is reported one team member should take the lead on [receiving the report], 
forming the response], 
and [communicating the response]. 

We recommend that another team member accompanies the lead responder as a quiet observer, when they receive the report, and communicate the response.
They can then help the responder remember details, give advice, and provide emotional support. 

We also recommend that the entire team has a chance to get clarifications, and provide feedback on the proposed response. 
We also recommend that a core organiser and core Forwards member join them. 
For this purpose, we recommend that the organisers inform the team if there is a quiet room that can use on demand.

#### After the event {#after-coc}

When the response team concludes their work, ideally up to 30 days after the event:

- Close any reports and anonymize them
- Write a transparency report with the information anonymized and share it with the Global Organisation Team
- Delete the event's CoC email email to avoid later data concerns, and all other data as recommended by Forwards [data retention policy](https://github.com/forwards/foundation/blob/7fe098d0fd82902c91449160487c90f768e4f39c/coc_policy/guidelines.md#data-retention-policy) draft.


<!-- Forwards is collecting annonimised reports from across events, to help prepare future response team. If you are happy to share your annonimised reports with us please send them to [coc-response@r-project.org](mailto:coc-response@r-project.org). -->

