# Governance and Decision Making {#governance}

The purpose of this document is to formalize the governance process used by the useR! Knowledgebase project to clarify how decisions are made and describe how the various members of our community interact.
This document establishes a decision-making structure that takes into account feedback from all members of the community and strives to find consensus while avoiding any deadlocks.

This is a meritocratic, consensus-based community project.
Anyone with an interest in the project can join the community, contribute to the project design and participate in the decision-making process.
This document describes how that participation takes place and how to go about earning merit within the project community.

## Contributors

Contributors are community members who contribute in concrete ways to the project. 
Anyone can become a contributor, and contributions can take many forms – not only code. 
More information will become available in the [contribution guide](#contributions).

## Maintainers Team

Maintainers are community members who have shown that they are dedicated to the continued development of the project through ongoing engagement with the community. 
They have shown they can be trusted to maintain useR! Knowledgebase with care. 
Being a maintainer allows the community to more easily carry on with their project-related activities by giving them direct access to the project’s repository and is represented as being an organization member on the useR! Knowledgebase GitLab organization. 
Maintainers are expected to review contributions, can merge approved pull requests, can cast votes for and against merging a pull request, and can be involved in deciding major changes to the project.

New maintainers can be nominated by any existing maintainers. 
Once they have been nominated, there will be a vote by the current maintainers. 
Voting on new maintainers is one of the few activities that take place on the project’s private management list.
While it is expected that most votes will be unanimous, a two-thirds majority of the cast votes is enough. 
The vote needs to be open for at least 1 week.

Maintainers that have not contributed to the project (commits or GitLab comments) in the past 12 months will be asked if they want to become emeritus maintainers and recant their commit and voting rights until they become active again. 
The list of maintainers, active and emeritus (with dates at which they became active) is public on the useR! Knowledgebase repository.

## Steering Committee

The Steering Committee (SC) members are maintainers who have additional responsibilities to ensure the smooth running of the project.
SC members are expected to participate in strategic planning and approve changes to the governance model.
The purpose of the SC is to ensure a smooth progress from the big-picture perspective.
Indeed changes that impact the full project require a careful analysis and a consensus that is both explicit and informed.
In cases that the maintainers community (which includes the SC members) fails to reach such a consensus in the required time frame, the SC is the entity to resolve the issue.

To ease the work of the SC, it was decided to keep it limited to 3 (three) members.
This way, discussions should be easy to coordinate and, if two members disagree, the third member can break the tie. 

Membership of the SC is made by nomination by a maintainer. 
A nomination will result in a discussion that cannot take more than a month and then a vote by the maintainers, which will stay open for a week. 
SC membership votes are subject to a two-thirds majority of all cast votes as well as a simple majority approval of all the current SC members.
SC members who do not actively engage with the SC duties are expected to resign.

Membership of the SC is limited to 2 (two) years term. 
After that period, a member would need to be nominated again by the maintainers community.
This process will give continuing members time to consider if they wish to continue and the maintainers an opportunity to allow new members to rotate into the SC. 

The Steering Committee of useR! Knowledgebase consists of: [Andrea Sánchez-Tapia](https://gitlab.com/andreasancheztapia), [Noa Tamir](https://gitlab.com/noatamir), and [Rocío Joo](https://gitlab.com/rociojoo).

## Decision-Making Process

Decisions about the future of the project are made through discussion with all members of the community. 
All non-sensitive project management discussion takes place on the issue tracker.
Occasionally, sensitive discussion occurs on a private slack.

useR! Knowledgebase uses a "consensus-seeking" process for making decisions.
The group tries to find a resolution that has no open objections among maintainers.
At any point during the discussion, any maintainers can call for a vote, which will conclude one month from the call for the vote.
Any call for a vote must be backed by a uKCP (useR! Knowledgebase Change Proposal). 
If no option can gather two-thirds of the votes cast, the decision is escalated to the SC, which in turn will use consensus seeking with the fallback option of a simple majority vote if no consensus can be found within a month.
This is what we hereafter may refer to as "the decision-making process".

Decisions (in addition to adding maintainers and SC membership as above) are made according to the following rules:

* Minor changes, such as typo fixes, or addition/correction of a sentence, require +1 by a maintainer and can be merged directly by them, happens on the issue or pull request page. 
* Major changes such as introducing new content, or significant editing of existing content, require +1 by one maintainer, no -1 by a maintainer (lazy consensus), happen on the issue or pull-request page.
Maintainers are expected to give "reasonable time" to others to give their opinion on the pull request if they’re not confident others would agree.
They are also expected to include maintainers who might have a say in the topic discussed, e.g. by tagging their username so they receive notifications and sharing the PR on slack discussion and team meetings.
* Significant changes to the project’s principles, technical changes, such as dependencies or supported versions, and any other decision with long-term implications on the project’s maintenance or mission happen via change proposals (uKCPs) and follow the decision-making process outlined above.
* Changes to the governance model use the same decision process outlined above.

If a veto -1 vote is cast on a lazy consensus, the proposer can appeal to the maintainers, and the change can be approved or rejected using the decision-making procedure outlined above.


## useR! knowledgebase change proposals (uKCPs)

For all votes, a proposal must have been made public and discussed before the vote. 
Such proposal must be a consolidated document, in the form of a ‘useR! Knowledgebase Changes Proposal’ (uKCP), rather than a long discussion on an issue.
A uKCP must be submitted as a pull request to enhancement proposals using the [uKCP template](https://gitlab.com/rconf/userknowledgebase/-/issues/79).
The proposal can be written by a contributor, a maintainer, or jointly by several people.

## Acknowledgments

This page is lightly adapted from [the scikit-learn project](https://scikit-learn.org/dev/governance.html#governance).
