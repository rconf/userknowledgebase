# useRknowledgebase
## A guide to hosting your first useR! Conference

The [useR! knowledgebase](https://rconf.gitlab.io/userknowledgebase) aims to be a manual with the cumulative knowledge useR! conference organizers have gathered during the editions of the conference, to guide future organizers and avoid them the need to "reinvent the wheel". 

This guide started as part of the 2021 [Google Season of Docs](https://github.com/rstats-gsod/gsod2021/wiki/useR!-knowledgebase).
Our aim is to create the central and authoritative source of documentation on the useR! conference.
By documenting thouroughly our steps, we also hope to be a useful guide to organizers of other conferences. 

## How to contribute

Corrections, suggestions, and general improvements are welcome as [issues](https://gitlab.com/rconf/userknowledgebase/-/issues).
You can also comment any of the existing issues.

### To build a local copy of this guide



+ Clone this git repo: 
`git clone https://gitlab.com/rconf/userknowledgebase`
+ Dependencies: you will need packages: __dplyr__, __kableExtra__, and __bookdown__.
+ The `index.Rmd` file at the root of the folder is the first page of the guide.
+ The chapters for the guide are `.Rmd` documents in numbered folders
+ By knitting `index.Rmd` locally, you can have a preview of the guide in the RStudio Viewer pane. You can edit other `.Rmd` files and knit them, this preview will only update the current file and is not a reflection of the current state of the whole guide.
+ To build the whole guide, execute `bookdown::render_book("index.Rmd")`. Navigate to `docs/` and open `index.html` in your web browser.

### To modify the content of this guide

+ Edit the `.Rmd` files. Bear in mind the first level header corresponds to chapters and second-level headers to sections. Some chapters have only one `.Rmd` file and others have multiple files with sections, these start with second-level headers. 
+ All images are in the `/assets` folder. They should be included in the Rmd files using R chunks and `knitr::include_graphics()`. The R chunk should include the alternative text for the figure, using parameter `fig.alt`
+ The structure of the guide is determined by the order in `_bookdown.yml`. When adding a chapter or a section, make sure to add it there too
+ Check locally that everything is how you want it and you're ready to push!

### To push your editions 
+ The only files you need to modify and commit/push are the .Rmd files, the images, and `_bookdown.yml`. __Please do not commit the locally-rendered files inside `docs/`__
+ You can push directly to `dev` branch for small fixes. 
+ For larger modifications, create branches per topic and refer to the issue(s) in your commits. After pushing the branch, create a __merge request__ to the `dev` branch. Try to limit each merge request to one chapter or topic. Target your merge requests to the `dev` branch.
+ The `main` branch is protected from accidental modification and saves the latest version. Merge requests from `dev` to `main` will be approved by the maintainers of the project.

## Code of conduct

This knowledgebase is bound by the __useR! working group code of conduct__. 
Everyone participating in the contruction and discussions is expected to abide by this code of conduct and show respect and courtesy to other community members at all times. 


## How to cite this knowledgebase 

To cite the useR! knowledgebase please use: 

Sánchez-Tapia, A., Tamir, N., Moy Das, S., Bellini Saibene, Y., Joo, R., Morandeira, N., Hug Peter, D., Vialaneix, N., Bannert, M., Turner, H., 2021. useR! Knowledgebase [WWW Document]. URL https://rconf.gitlab.io/userknowledgebase/ (date of access).


```
@misc{sanchez-tapia_user_2021-2,
	author = {{S{\'a}nchez-Tapia}, Andrea and Tamir, Noa and Moy Das, Sangeet and Bellini Saibene, Yanina and Joo, Roc{\'\i}o and Morandeira, Natalia and Hug Peter, Dorothea and Vialaneix, Nathalie and Bannert, Matt and Turner, Heather},
	howpublished = {https://rconf.gitlab.io/userknowledgebase/},
	title = {{{useR}}! {{Knowledgebase}}},
	year = {2021}}

```


